# HomeDir Skeleton Files

## Install on a new machine
1. `git clone --bare git@gitlab.com:vandarin/config-skeleton.git $HOME/.homecfg`
1. `alias config='/usr/bin/git --git-dir=$HOME/.homecfg/ --work-tree=$HOME'`
1. `config checkout`


## Features
- config() alias function for git, that sets working dir to $HOME, gitdir to ~/.homecfg (config add, config commit, etc.)
- git branch name in command prompt
- ls colors by default
- grep colors by default
- Pygments (colorizes less)
- Local path files (.bash_paths)
- Bash completion (mac homebrew)
- oc Bash completion
- sane defaults for .gitconfig, includes .gitconfig-custom for user-specific directives
- "standard" git aliases


**Reference**: https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/

All files are ignored by default, add with `git add -f {FILE}`