# Load platform- and tool-specific PATHs
[ -f ~/.bash_paths ] && . ~/.bash_paths
export PATH=${PATH}:~/bin

if [ -f ~/.bashrc ]; then
   source ~/.bashrc
fi

export SHELL_SESSION_HISTORY=0
export EDITOR=nano

if [[ `which pygmentize` ]]; then
	#less syntax highlighting
	export LESSOPEN='| pygmentize -f terminal -O style=native -g %s'

	# https://superuser.com/questions/1346235/less-seems-to-no-longer-accept-mouse-scrolls-in-the-terminal-in-macos
	#  -F : automatically exit for small files
	export LESS='-R'
	export MANPAGER="less -is"
fi

## Git branch in prompt
parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

function config {
   git --git-dir=$HOME/.homecfg/ --work-tree=$HOME "$@"
}

function homestead() {
    ( cd ~/Homestead && vagrant $* )
}

## Tab Completion
[ -f /usr/local/etc/bash_completion ] && source /usr/local/etc/bash_completion
[ -f ~/bin/oc_completion.sh ] &&  source ~/bin/oc_completion.sh

## Aliases
which dircolors > /dev/null 2>&1 /dev/null && eval "`dircolors`"

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     alias ls="ls --color=auto";;
    Darwin*)    alias ls="ls -G";;
esac
alias grep='grep --color'
alias gdc='git diff --cached'

## Command prompt template
if [[ -f ~/.bash-powerline.sh ]]; then
	source ~/.bash-powerline.sh
else
	PS1="\[\033[01;32m\]\u@\h \[\033[01;34m\]\w\[\033[33m\]\$(parse_git_branch)\[\033[00m\] \$ "
fi
