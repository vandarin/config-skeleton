PFX=$1
[[ -z "$PFX" ]] && echo "usage: convert-pfx.sh <filename.pfx>" && exit 1
openssl pkcs12 -nokeys -in "$PFX" -out "${PFX%.pfx}.pem"
openssl pkcs12 -nocerts -nodes -in "$PFX" -out "${PFX%.pfx}.key"
