#!/bin/bash
set -e
git checkout master
git fetch --prune
git merge --ff-only origin/master
for branch in $(git branch | grep -v master)
do
	git rebase master $branch || git rebase --abort
done

git checkout master

