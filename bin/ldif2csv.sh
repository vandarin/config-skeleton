#!/bin/bash
#
# Converts LDIF data to CSV.
# Doesn't handle comments very well. Use -LLL with ldapsearch to remove them.
#
# 2010-03-07
# dsimmons@squiz.co.uk
#

# Show usage if we don't have the right params
if [ "$1" == "" ]; then
	echo ""
	echo "Usage: cat ldif.txt | $0 <attributes> [...]"
	echo "Where <attributes> contains a list of space-separated attributes to include in the CSV. LDIF data is read from stdin."
	echo ""
	exit 99
fi

ATTRS="$*"

c=0
while read line; do

	# Skip LDIF comments
	[ "${line:0:1}" == "#" ] && continue;

	# If this line is blank then it's the end of this record, and the beginning
	# of a new one.
	#
	if [ "$line" == "" ]; then

		output=""

		# Output the CSV record
		for i in $ATTRS; do

			eval data=\$RECORD_${i}
			output=${output}\"${data}\",

			unset RECORD_${i}

		done

		# Remove trailing ',' and echo the output
		output=${output%,}
		echo $output

		# Increase the counter
		c=$(($c+1))
	fi

	# Separate attribute name/value at the semicolon (LDIF format)
	attr=${line%%:*}
	value=${line#*: }

	# Save all the attributes in variables for now (ie. buffer), because the data
	# isn't necessarily in a set order.
	#
	for i in $ATTRS; do
    if [ "$attr" == "$i" ]; then
        eval tmp=$(echo $(echo \$RECORD_${attr}))
        if [[ ! -z "$tmp" ]]; then
            eval RECORD_${attr}=$tmp':'\"$value\"
        else
            eval RECORD_${attr}=\"$value\"
        fi
    fi
done

done
