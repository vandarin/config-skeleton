date

FILEARG=$1
SCRIPT=/Users/lane/Nextcloud/LR\ Makes/OpenSCAD/StampFromSVG.scad
if [[ $FILEARG == 'flat' ]]; then
FILEARG=$2
SCRIPT=/Users/lane/Nextcloud/LR\ Makes/OpenSCAD/FlatStamp.scad
fi
INPUT="$(cd "$(dirname "$FILEARG")"; pwd -P)/$(basename "$FILEARG")"
DIR=$(dirname "$INPUT")
FILE=$(basename "$INPUT")
echo "Converting $INPUT"

/Applications/OpenSCAD.app/Contents/MacOS/OpenSCAD "$SCRIPT" -D "FILENAME=\"$INPUT\"" -o "$DIR/$FILE.stl"

date
