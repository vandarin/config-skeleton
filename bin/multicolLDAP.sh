#!/bin/bash
echo
USERS=0
NAMES=0
ATTRS="pid mail givenname sn departmentnumber"
LDAP_BASE="dc=global,dc=ul,dc=com"
LDAP_HOST="USBENADDS001P.global.ul.com"

if (($# == 0)); then
  echo "Usage: $0 -f filename [-a attributes]"
  echo
  exit 1
fi

while getopts ":f:a:" opt; do
  case ${opt} in
    a ) # process option a
		ATTRS="${OPTARG}"
      ;;
    f ) # process option t
		FILE="${OPTARG}"
      ;;
    \? ) echo "Usage: $0 -f filename [-a] attributes"
		exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

# read -s -p "Enter AD Password: " LDAP_PASS

while read i; do
	let USERS++
	DISP=`ldapsearch -LLL -D "24786@global.ul.com" -w "$LDAP_PASS" -h "$LDAP_HOST" -x -v -b "$LDAP_BASE" "mail=$i" $ATTRS | ldif2csv.sh $ATTRS`
	echo "$DISP"
done < "${FILE}"

echo
